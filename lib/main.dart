import 'package:flutter/material.dart';
import 'package:x_change/routes.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:x_change/screens/sign_in/sign_in_screen.dart';
import 'package:x_change/screens/slider/getstarted.dart';
import 'package:x_change/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: theme(),
      home: Pview(),
      // We use routeName so that we dont need to remember the name
      routes: routes,
    );
  }
}
