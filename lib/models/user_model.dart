import 'package:flutter/material.dart';




class UserModel{

  String? email;
  String? firstName;
  String? lastName;
  String? uid;

  UserModel({this.email,this.firstName,this.lastName,this.uid});

  //data form server

  factory UserModel.fromMap(map){
    return UserModel(
      uid: map['uid'],
      email: map['email'],
      firstName: map['firstName'],
      lastName: map['lastName'],
    );
  }


  //sending data to server

  Map<String,dynamic> toMap(){
    return {
      'uid': uid,
      'email':email,
      'firstName':firstName,
      'lastName':lastName,
    };
  }





}