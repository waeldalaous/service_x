import 'dart:async';

import 'package:flutter/material.dart';
import 'package:x_change/constants.dart';

class Data {
  final String description;
  final String imageUrl;

  Data({
    required this.description,
    required this.imageUrl,
  });
}

// ignore: camel_case_types
class indicator extends StatelessWidget {
  final int index;

  const indicator(this.index);
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: const Alignment(0, 0.7),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BuildContainer(
            0,
            index == 0
                ? Color.fromARGB(255, 10, 0, 130)
                : Color.fromARGB(255, 55, 134, 150),
          ),
          BuildContainer(
            1,
            index == 1
                ? Color.fromARGB(255, 10, 0, 130)
                : Color.fromARGB(255, 55, 134, 150),
          ),
          BuildContainer(
            2,
            index == 2
                ? Color.fromARGB(255, 10, 0, 130)
                : Color.fromARGB(255, 55, 134, 150),
          ),
          BuildContainer(
            3,
            index == 3
                ? Color.fromARGB(255, 10, 0, 130)
                : Color.fromARGB(255, 55, 134, 150),
          ),
        ],
      ),
    );
  }

  // ignore: non_constant_identifier_names
  Widget BuildContainer(int i, Color color) {
    return index == i
        ? Container(
            margin: EdgeInsets.all(4),
            height: 6,
            width: 6,
            decoration: BoxDecoration(
              color: color,
              shape: BoxShape.circle,
            ),
          )
        : Container(
            margin: EdgeInsets.all(4),
            height: 8,
            width: 8,
            decoration: BoxDecoration(
              color: color,
              shape: BoxShape.circle,
            ),
          );
  }
}

class Pview extends StatefulWidget {
  const Pview({Key? key}) : super(key: key);

  @override
  _PviewState createState() => _PviewState();
}

class _PviewState extends State<Pview> {
  int _currentindex = 0;
  final _pageIndexNotifier = ValueNotifier<int>(0);
  final PageController _controller = PageController(
    initialPage: 0,
  );

  final List<Data> myData = [
    Data(
      description: "",
      imageUrl: "assets/images/network.png",
    ),
    Data(
      description: "",
      imageUrl: "assets/images/save-time.png",
    ),
    Data(
      description: "",
      imageUrl: "assets/images/help.png",
    ),
    Data(
      description: "",
      imageUrl: "assets/images/pencil.png",
    )
  ];

  @override
  void initState() {
    super.initState();
    Timer.periodic(const Duration(seconds: 4), (timer) {
      if (_currentindex < 3) {
        _currentindex++;
      }
      _controller.animateToPage(_currentindex,
          duration: const Duration(milliseconds: 1000), curve: Curves.easeIn);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            kPrimaryColor,
            Color.fromARGB(255, 170, 205, 239),
            Color.fromARGB(255, 107, 167, 227),
            
          ],
        ),
      ),
      child: Stack(
        children: [
          Builder(
            builder: (ctx) => PageView(
              children: myData
                  .map((item) => Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                          alignment: Alignment.center,
                          image: ExactAssetImage(item.imageUrl),
                          fit: BoxFit.contain,
                        )),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const SizedBox(height: 500),
                              Text(
                                item.description,
                                style: TextStyle(
                                  inherit: false,
                                  color: Color.fromARGB(255, 10, 0, 130),
                                  fontWeight: FontWeight.w500,
                                  fontSize: 20,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                      ))
                  .toList(),
              onPageChanged: (val) {
                _pageIndexNotifier.value = val;
                setState(() {
                  _currentindex = val;
                  // if (_currentindex == 3) {
                  //   Future.delayed(Duration(seconds: 3),
                  //       () => Navigator.of(ctx).pushNamed('/a'));
                  // }
                });
              },
              controller: _controller,
            ),
          ),
          indicator(_currentindex),
          Builder(
            builder: (ctx) => Align(
              alignment: const Alignment(0, 0.9),
              child: Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 25,
                ),
                child: ConstrainedBox(
                  constraints: BoxConstraints.tightFor(width: 250, height: 58),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0))),
                      backgroundColor: MaterialStateProperty.all(
                          Color.fromARGB(255, 10, 0, 130)),
                      padding:
                          MaterialStateProperty.all(const EdgeInsets.all(12)),
                    ),
                    child: const Text(
                      "commencer",
                      style: TextStyle(
                        color: Color.fromARGB(255, 190, 210, 224),
                        fontSize: 25,
                      ),
                    ),
                    onPressed: () async {
                      Navigator.of(ctx).pushReplacementNamed('/sign_in');
                    },
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
