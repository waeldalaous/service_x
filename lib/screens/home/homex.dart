import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:x_change/screens/ajouterPubs/ajouter_publication.dart';
import 'package:x_change/screens/mesPubs/mes_publicationsx.dart';
import 'package:x_change/screens/profile/profilx.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:x_change/constants.dart';




class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final postsRef = FirebaseFirestore.instance.collection('posts');
  @override
  void initState(){

    getPosts(    );
    super.initState();
  }
  getPosts(){
    postsRef.get().then((QuerySnapshot snapshot) {
      snapshot.docs.forEach((DocumentSnapshot doc) {
        print(doc.data);
      }
      );

    }
    );

  }
  
  int _currentIndex=0;
  Icon customIcon = const Icon(Icons.search);
  Widget customSearchBar = const Text('');
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: customSearchBar,
        actions: <Widget>[
          IconButton(
            iconSize: 30.0,
            icon: customIcon,
            onPressed: () {
              setState(() {
                if (customIcon.icon == Icons.search) {
                  customIcon = const Icon(Icons.cancel);
                  customSearchBar = Expanded(
                    child: ListTile(
                      leading: Icon(
                        Icons.search,
                        color: Colors.white,
                        size: 28,
                      ),
                      title: Container(
                        height: 40,
                        width: 30,
                        child: TextField(
                          decoration: InputDecoration(
                            hintText: 'Taper le service recherché',
                            hintStyle: TextStyle(
                              color: Colors.black12,
                              fontSize: 18,
                              fontStyle: FontStyle.italic,
                            ),
                            border: InputBorder.none,
                          ),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  );
                  // Perform set of instructions.
                } else {
                  customIcon = const Icon(Icons.search);
                  customSearchBar = const Text('');
                }
              });
            },
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.shifting,
        backgroundColor: bottomAppBarColor,
        selectedIconTheme: iconTheme,
        unselectedIconTheme: primaryIconTheme,
        iconSize: 30.0,
        selectedItemColor: primaryColorDark,
        unselectedItemColor: Colors.blue,
        selectedLabelStyle: TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
        ),
        currentIndex: _currentIndex,
        onTap: (index){
          setState(() {
            _currentIndex = index;
            if (_currentIndex == 0) {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyHomePage(title: 'MyHomePage'))
              );
            } else if (_currentIndex == 1) {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyPubsPage(title: 'MyPubsPage')));
            } else if (_currentIndex == 2) {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyProfilePage(title: 'MyProfilePage')));
            }
          }
          );
        },


        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            label: 'Accueil',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.folder,
            ),
            label: 'Mes pubs',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            label: 'Profil',
          ),
        ],

      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {  Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const MyAddPubPage(title: 'MyAddPubPage'))
        );
        },
        backgroundColor: cardColor,
        child: const Icon(Icons.add, size: 28.0,),
      ),
      body: Center(
        child: Column(
          children: [
             Padding(
              padding:  EdgeInsets.all(2),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Recommandations',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: 'Source Sans Pro',
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      color: canvasColor,

                    ),

                  ),
                  ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      primary :Colors.white,
                      elevation: 0,
                    ),
                    child: Icon(  Icons.filter_alt_outlined, size: 25,color: Color(0xff00003c)),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: ListView(
                children:  [
                  Expanded(
                    flex: 1,
                    child: Container(
                      height: 150.0,
                      margin: const EdgeInsets.all(10.0),
                      padding: const EdgeInsets.all(1.0),

                      decoration: BoxDecoration(
                          color: Color(0xff00003c),
                          borderRadius: BorderRadius.all(Radius.circular(20))
                      ),


                      child: Card(
                        color: Color(0xff00003c),
                        margin:const EdgeInsets.all(6.0),
                        child: Row(
                          children: [
                            const Padding(
                              padding:  EdgeInsets.all(8.0),
                              child: CircleAvatar(
                                backgroundImage: AssetImage('assets/images/Profile Image.png'),
                                radius: 70.0,
                              ),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [

                                Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Text('Mariem Kammoun',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                      fontFamily: 'Kalam',
                                      fontStyle: FontStyle.italic,


                                    ),),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: Text('Baby-sitting',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17.0,
                                      fontFamily: 'Source Sans Pro',
                                      fontStyle: FontStyle.normal,

                                    ),),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(2.0),
                                  child: Text('10dt/heure',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 17.0,
                                      fontFamily: 'Source Sans Pro',
                                      fontStyle: FontStyle.normal,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding:EdgeInsets.fromLTRB(0, 1, 8, 1) ,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(Radius.circular(20))
                                  ),

                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                                    children: [
                                      
                                      Text(
                                        '50',
                                        style: TextStyle(
                                          color: primaryColorDark,
                                          fontSize: 20.0,
                                          fontFamily: 'Source Sans Pro',
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.bold,
                                        ),

                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
