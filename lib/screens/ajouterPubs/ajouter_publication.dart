import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:x_change/theme1.dart';
import 'package:x_change/screens/mesPubs/mes_publicationsx.dart';
import 'package:x_change/screens/home/homex.dart';
import 'package:x_change/screens/profile/profilx.dart';

import '../../constants.dart';


class MyAddPubPage extends StatefulWidget {
  const MyAddPubPage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyAddPubPage> createState() => _MyAddPubPageState();
}

class _MyAddPubPageState extends State<MyAddPubPage> {
  int _currentIndex=0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          iconSize: 30.0,
          icon: const Icon(Icons.arrow_back,),
          onPressed: () {Navigator.of(context).pop();},
        ),
        actions: <Widget>[
          Row(
            children: [
              Text('50',
                 style: TextStyle(
                   fontSize: 25.0,
                   fontFamily: 'Source Sans Pro',
                   fontStyle: FontStyle.italic,
                   fontWeight: FontWeight.bold,
                   color: Colors.white,

                 ),
              ),
              IconButton(
                iconSize: 30.0,
                icon: const Icon(Icons.attach_money),
                onPressed: () {},
              ),
            ],
          ),
        ],
      ),

      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding:  EdgeInsets.all(8),
                  child:  Text(
                    'Entrer les informations relatives à votre service',
                    style: TextStyle(
                      fontSize: 15.0,
                      fontFamily: 'Source Sans Pro',
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      color: canvasColor,

                    ),
                  ),
                ),
                Container(

                  margin: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                  padding: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                  child: const TextField(

                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Titre',
                    ),
                  ),
                ),
                Container(

                  margin: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                  padding: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                  child: const TextField(

                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                      ),
                      labelText: 'Prix',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 260,
                        padding: const EdgeInsets.all(8.0),
                        child: const TextField(

                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Date',
                          ),
                        ),
                      ),
                      IconButton(
                        color: primaryColorDark,
                        iconSize: 50,
                        icon: Icon(
                          Icons.date_range,
                        ),
                        onPressed: () {},
                     ),

                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 2, 15, 2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 260,
                        padding: const EdgeInsets.all(8.0),
                        child: const TextField(

                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Localisation',
                          ),
                        ),
                      ),
                      IconButton(
                        color: primaryColorDark,
                        iconSize: 50,
                        icon: const Icon(
                          Icons.add_location,
                        ),
                        onPressed: () { },

                      ),

                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(15, 2, 15, 2),

                  margin: const EdgeInsets.fromLTRB(15, 2, 15, 2),

                  child: const TextField(

                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                      ),
                      labelText: 'Description',
                    ),
                  ),
                ),
                Container(
                  width: 100,
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    color: primaryColorDark,
                    iconSize: 50,
                  icon: const Icon(
                    Icons.add_photo_alternate,
                  ),
                  onPressed: () { },
                  ),
                ),
                ElevatedButton(
                  onPressed: () {

                  },
                  style: ElevatedButton.styleFrom(
                    primary :Color(0xff00003c),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                  ),
                  child:Text('Publier',
                    style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: 'Source Sans Pro',
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,

                    ),
                  ),
                ),


              ],
            ),
          ),
        ),
      ),
    );
  }
}
