import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:x_change/models/user_model.dart';
import 'package:x_change/screens/mesPubs/mes_publicationsx.dart';
import 'package:x_change/screens/home//homex.dart';
import 'package:x_change/screens/profile/components/profile_pic.dart';
import 'package:x_change/screens/profile/profile_screen.dart';

import '../../constants.dart';


class MyProfilePage extends StatefulWidget {
  const MyProfilePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyProfilePage> createState() => _MyProfilePageState();
}

class _MyProfilePageState extends State<MyProfilePage> {
  int _currentIndex=2;
  User? user = FirebaseAuth.instance.currentUser;
  UserModel loggedInUser = UserModel() ;
  @override
  void initState() {
    super.initState();
    FirebaseFirestore.instance
        .collection("users")
        .doc(user!.uid)
        .get()
        .then((value) {
          this.loggedInUser = UserModel.fromMap(value.data());
          setState(() {

          });
        });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: Colors.white,
      appBar: AppBar(

        leading: IconButton(
          iconSize: 30.0,
          icon: const Icon(Icons.arrow_back,),
          onPressed: () {Navigator.of(context).pop();},
        ),
        actions: <Widget>[
          IconButton(
            iconSize: 30.0,
            icon: const Icon(Icons.menu),
            onPressed: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>ProfileScreen()));
            },),

        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index){
          setState(() {
            _currentIndex = index;
            if (_currentIndex == 0) {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyHomePage(title: 'MyHomePage'))
              );
            } else if (_currentIndex == 1) {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyPubsPage(title: 'MyPubsPage')));
            } else if (_currentIndex == 2) {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyProfilePage(title: 'MyProfilePage')));
            }
          }
          );
        },
        type: BottomNavigationBarType.shifting,
        backgroundColor: bottomAppBarColor,
        selectedIconTheme: iconTheme,
        unselectedIconTheme: primaryIconTheme,
        iconSize: 30.0,
        selectedItemColor: primaryColorDark,
        unselectedItemColor: Colors.white,
        selectedLabelStyle: TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
        ),

        items:  <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            label: 'Accueil',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.folder,
            ),
            label: 'Mes pubs',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            label: 'Profil',
          ),
        ],

      ),


      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 0,
                        child: ProfilePic(),
                      ),
                      Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text("${loggedInUser.firstName} ${loggedInUser.lastName}",
                              style: TextStyle(
                                color: Color(0xff00003c),
                                fontSize:25.0,
                                fontFamily: 'Kalam-Light',
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(' Crédit : 120X ',
                              style: TextStyle(
                                color: Color(0xff00003c),
                                fontSize:25.0,
                                fontFamily: 'Source Sans Pro',
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),

              Expanded(
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  padding: const EdgeInsets.all(1.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Color(0xff00003c),
                          width: 2,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(20))
            ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Mes compétences',
                            style: TextStyle(
                              color: Color(0xff00003c),
                              fontSize: 20.0,
                              fontFamily: 'Source Sans Pro ',
                              fontStyle: FontStyle.italic,
                              fontWeight: FontWeight.bold,
                            ),
                        ),
                      ),
                      Expanded(
                        child: ListView(
                          children: [
                            Container(

                              margin: const EdgeInsets.all(10.0),
                              padding: const EdgeInsets.all(1.0),
                              decoration: BoxDecoration(
                                color: Color(0xff00003c),
                                borderRadius: BorderRadius.all(Radius.circular(20),
                                ),
                              ),
                              child: Card(
                                color: Color(0xff00003c),
                                margin:const EdgeInsets.all(6.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                     Text('Projet de développement', style: TextStyle(
                                       color: Colors.white,
                                       fontSize: 18.0,
                                       fontFamily: 'Kalam',
                                       fontStyle: FontStyle.italic,
                                     ),),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        Container(
                                          padding:EdgeInsets.fromLTRB(0, 1, 8, 1),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.all(Radius.circular(20))
                                          ),

                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                                            children: [
                                              Text(
                                                '83',
                                                style: TextStyle(
                                                  color: primaryColorDark,
                                                  fontSize: 20.0,
                                                  fontFamily: 'Source Sans Pro',
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),

                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 0,
                              child: Container(

                                margin: const EdgeInsets.all(10.0),
                                padding: const EdgeInsets.all(1.0),
                                decoration: BoxDecoration(
                                  color: Color(0xff00003c),
                                  borderRadius: BorderRadius.all(Radius.circular(20),
                                  ),
                                ),
                                child: Card(
                                  color: Color(0xff00003c),
                                  margin:const EdgeInsets.all(6.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('Baby-sitting', style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                        fontFamily: 'Kalam',
                                        fontStyle: FontStyle.italic,
                                      ),),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        children: [
                                          Container(
                                            padding:EdgeInsets.fromLTRB(0, 1, 8, 1),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: BorderRadius.all(Radius.circular(20))
                                            ),

                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                                              children: [
                                                Text(
                                                  '50',
                                                  style: TextStyle(
                                                    color: Theme.of(context).primaryColorDark,
                                                    fontSize: 20.0,
                                                    fontFamily: 'Source Sans Pro',
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),

                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      ElevatedButton(
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          primary :Color(0xff008ca0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                          ),
                        ),
                        child: Icon(Icons.add, size: 28,),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
