import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:x_change/screens/ajouterPubs/ajouter_publication.dart';
import 'package:x_change/screens/home/homex.dart';
import 'package:x_change/screens/profile/profilx.dart';

import '../../constants.dart';


class MyPubsPage extends StatefulWidget {
  const MyPubsPage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyPubsPage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyPubsPage> {
  int _currentIndex=1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          iconSize: 30.0,
          icon: const Icon(Icons.arrow_back,),
            onPressed: () {Navigator.of(context).pop();}
        ),
        actions: <Widget>[
          IconButton(
            iconSize: 30.0,
            icon: const Icon(Icons.attach_money),
            onPressed: () {}
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index){
          setState(() {
            _currentIndex = index;
            if (_currentIndex == 0) {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyHomePage(title: 'MyHomePage'))
              );
            } else if (_currentIndex == 1) {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyPubsPage(title: 'MyPubsPage')));
            } else if (_currentIndex == 2) {
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const MyProfilePage(title: 'MyProfilePage')));
            }
          }
          );
        },



        type: BottomNavigationBarType.shifting,
        backgroundColor: bottomAppBarColor,
        selectedIconTheme: iconTheme,
        unselectedIconTheme: primaryIconTheme,
        iconSize: 30.0,
        selectedItemColor: primaryColorDark,
        unselectedItemColor: Colors.white,
        selectedLabelStyle: TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
        ),

        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
            ),
            label: 'Accueil',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.folder,
            ),
            label: 'Mes pubs',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            label: 'Profil',
          ),
        ],

      ),

      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Expanded(
            child: Container(
              margin: const EdgeInsets.all(10.0),
              padding: const EdgeInsets.all(1.0),

              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Mes publications',
                      style: TextStyle(
                        color: Color(0xff00003c),
                        fontSize: 24.0,
                        fontFamily: 'Source Sans'
                            ' Pro ',
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(8),
                          padding: const EdgeInsets.fromLTRB(0, 0,0, 10),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                color: Color(0xff00003c),
                                width: 2,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(22))
                          ),
                          child: Column(
                            children: [
                              Container(

                                margin: const EdgeInsets.all(0),
                                padding: const EdgeInsets.all(0),
                                decoration: BoxDecoration(
                                  color: Color(0xff00003c),
                                  borderRadius: BorderRadius.all(Radius.circular(20),
                                  ),
                                ),
                                child: Card(
                                  color: Color(0xff00003c),
                                  margin:const EdgeInsets.all(6.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Text('Baby-Sitting', style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18.0,
                                            fontFamily: 'Kalam',
                                            fontStyle: FontStyle.italic,
                                          ),
                                          ),
                                          Text('22/01/2022', style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18.0,
                                            fontFamily: 'Source Sans Pro',

                                          ),
                                          ),

                                        ],
                                      ),
                                      Container(
                                        padding:EdgeInsets.all(2),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.all(Radius.circular(10))
                                        ),

                                        child: Text(
                                          '10 X/h',
                                          style: TextStyle(
                                            color: primaryColorDark,
                                            fontSize: 18.0,
                                            fontFamily: 'Source Sans Pro',
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.bold,
                                          ),

                                        ),
                                      ),
                                      ElevatedButton(
                                        onPressed: () {},
                                        style: ElevatedButton.styleFrom(
                                          primary :Color(0xff00003c),

                                        ),
                                        child: Icon(Icons.menu, size: 20,),
                                      ),

                                    ],
                                  ),
                                ),

                              ),
                              Text(
                                '3 demandes',
                                style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 18.0,
                                  fontFamily: 'Source Sans Pro',
                                  fontStyle: FontStyle.italic,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),

                        ),


                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: FloatingActionButton(
                      onPressed: () {  Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const MyAddPubPage(title: 'MyAddPubPage'))
                      );
                      },
                      backgroundColor: cardColor,
                      child: const Icon(Icons.add, size: 28.0,),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
