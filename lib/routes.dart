import 'package:flutter/widgets.dart';
import 'package:x_change/screens/forgot_password/forgot_password_screen.dart';
import 'package:x_change/screens/profile/profile_screen.dart';
import 'package:x_change/screens/sign_in/sign_in_screen.dart';



import 'screens/sign_up/sign_up_screen.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {

  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
};
