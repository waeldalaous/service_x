import 'package:flutter/material.dart';



const kPrimaryColor = Color(0xff000066);
const kPrimaryLightColor = Color(0xFFFFECDF);
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],

);
const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);
const bottomAppBarColor = Color(0xff00003c);
const kAnimationDuration = Duration(milliseconds: 200);
const primarySwatch = MaterialColor(4278190140, {
50: Color(0xffe5e5ff),
100: Color(0xffccccff),
200: Color(0xff9999ff),
300: Color(0xff6666ff),
400: Color(0xff3333ff),
500: Color(0xff0000ff),
600: Color(0xff0000cc),
700: Color(0xff000099),
800: Color(0xff000066),
900: Color(0xff000033)
});
const iconTheme = IconThemeData(
color:Color(0xff008ca0),
);
const primaryIconTheme= IconThemeData(
color: Color(0xFF757575),
);
const primaryColorDark = const Color(0xff008ca0);
const cardColor=  Color(0xff008ca0);
const canvasColor= Color(0xff00003c);
final headingStyle = TextStyle(
  fontSize: 28,
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

const defaultDuration = Duration(milliseconds: 250);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "Please Enter your email";
const String kInvalidEmailError = "Please Enter Valid Email";
const String kPassNullError = "Please Enter your password";
const String kShortPassError = "Password is too short";
const String kMatchPassError = "Passwords don't match";
const String kFirstNameNullError = "Please Enter your first name";
const String kLastNamelNullError = "Please Enter your last name";

